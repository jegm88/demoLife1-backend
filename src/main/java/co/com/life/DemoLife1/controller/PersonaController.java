package co.com.life.DemoLife1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.life.DemoLife1.model.Persona;
import co.com.life.DemoLife1.repository.PersonaRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/persona")
@Api(value = "Persona", tags = {"Persona"})
@CrossOrigin("*")
public class PersonaController {

	@Autowired
	private PersonaRepository personaRepository;
	
	@ApiOperation(value = "Guarda una persona en la base de datos.")
	@RequestMapping(method = RequestMethod.POST)
    public Persona save(@RequestBody Persona persona) {
		return personaRepository.save(persona);
    }
	
	@ApiOperation(value = "Consula una persona de la base de datos a partir de su id.")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Persona findById(@PathVariable("id") Long id) {
		return personaRepository.findOne(id);
    }

	@ApiOperation(value = "Elimina una persona de la base de datos a partir de su id.")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
		personaRepository.delete(id);
    }
	
	@ApiOperation(value = "Lista las persona de la base de datos.")
	@RequestMapping(method = RequestMethod.GET)
    public Iterable<Persona> findAll() {
		return personaRepository.findAll();
    }
	
}

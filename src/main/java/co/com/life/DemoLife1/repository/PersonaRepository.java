package co.com.life.DemoLife1.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import co.com.life.DemoLife1.model.Persona;

public interface PersonaRepository  extends PagingAndSortingRepository<Persona, Long> {

}
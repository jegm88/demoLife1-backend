package co.com.life.DemoLife1.model.type;

public enum TipoDocumento {
	CC("Cédula de ciudadanía"),
	CE("Cédula de extrangería"),
	TI("Tarjeta de identidad"),
	ND("No disponible");
	
	private String valor;
	
	TipoDocumento(String valor) {
		this.valor = valor;
	}

	public String getValor() {
		return valor;
	}
}

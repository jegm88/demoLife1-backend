package co.com.life.DemoLife1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoLife1Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoLife1Application.class, args);
	}
}
